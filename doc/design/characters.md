# Character

## License

The reference images below are either part of the public domain,
or licensed under a Creative Commons license,
(See the top-level README.md file for more information).

## Reference

- [Amazing Adventures No. 1](https://comicbookplus.com/?dlid=1346)

- [Space Adventures No. 7](https://comicbookplus.com/?dlid=62549)

- [Cyberpunk City.jpg](https://commons.wikimedia.org/wiki/File:Cyberpunk_City.jpg)
