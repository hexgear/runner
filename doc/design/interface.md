# Interface 

## Design Specifications

Target screen resolution is `768` x `1366` pixels for mobile devices.

Primary color is `#cc00ff`.

## Dependencies

- [draw.io](https://www.diagrams.net)
